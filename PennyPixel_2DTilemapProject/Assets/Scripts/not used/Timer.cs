﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    bool finished = false;
    float time;

    public Timer(float time)
    {
        this.time = time;
    }

    // Update is called once per frame
    void Update()
    {
        time -= Time.deltaTime;
        if(time <= 0)
        {
            finished = true;
        }
    }

    bool isFinished()
    {
        return finished;
    }
}
