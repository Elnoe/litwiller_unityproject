﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalMover : MonoBehaviour
{
    public float transformYRate = .01f;
    public float maxDistance = 10f;
    protected float yInit;
    protected bool direction;

    // Start is called before the first frame update
    void Start()
    {
        yInit = transform.localPosition.y;
        direction = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (direction) //move up
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + transformYRate, transform.localPosition.z);
        }
        else //move down
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - transformYRate, transform.localPosition.z);
        }

        if (transform.localPosition.y > yInit + maxDistance || transform.localPosition.y < yInit) //check if the top or bottom bounds have been reached
        {
            direction = !direction;
        }
    }
}
