﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FireController : MonoBehaviour
{
    protected float size = 22f; //Stores the fire size over time
    protected float growRate = 1.8f; //How fast the fire will grow
    public Text loseText;
    public Text restartText;
    public Text runText;
    protected float gameOverSlowMo; //The time the player spends in slow-mo after losing
    protected bool isGameOver;
    protected int count;

    // Start is called before the first frame update
    void Start()
    {
        loseText.text = "";
        restartText.text = "";
        runText.text = "Run!";
        gameOverSlowMo = 3f;
        isGameOver = false;
        Time.timeScale = 1f;
        count = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (count < 180)
        {
            count++;
        } else
        {
            runText.text = "";
        }

        size += Time.deltaTime * growRate; //Increase the size over time
        transform.localScale = new Vector3(size, size, transform.localScale.z); //Change the size of sphere, but only in x and y  
        //transform.localPosition = new Vector3(size, transform.localPosition.x, transform.localPosition.z); //Change the position of sphere, but only in x 

        float sphereEdge = -42.75f + 0.5f * size;

        if (Mathf.Abs(PlayerPlatformerController.playerTransform.localPosition.x - sphereEdge) < 0.1 || PlayerPlatformerController.playerTransform.localPosition.y <= -9) //If the player and the edge of the sphere are <0.1 apart, begin the gameOver process
        {
            Time.timeScale = 0.2f;
            loseText.text = "Game Over!";
           
            isGameOver = true;
        }

        if (isGameOver) //Calculate the remaining slow-mo time
        {
            gameOverSlowMo -= Time.deltaTime * 10;
        }

        if(gameOverSlowMo <= 0) //When the slow-mo timer ends, freeze the game
        {
           Time.timeScale = 0f;
           restartText.text = "Press [SPACE] to restart";
           if (Input.GetKeyDown("space"))
           {
               SceneManager.LoadScene("Final");
           }
        }
    }
}
