﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPlatformerController : PhysicsObject
{ 
    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;

    protected bool isPlayingWin; //Is the win sound effect playing?
    protected bool isPlayingJump; //Is the jump rune pickup sound playing?
    protected bool isPlayingSpeed; //Is the speed rune pickup sound playing?

    public Text effectsText; //Status effects and timers in upper left of HUD
    protected string effectsString;
    protected float currJumpTime; //Time remaining of the jump effect
    protected float currSpeedTime; //Time remaining of the speed effect

    private SpriteRenderer spriteRenderer;
    private Animator animator;
    public static Transform playerTransform;
    protected AudioSource audioSource;

    protected const float runeTime = 5f; //How long a rune effect lasts

    protected GameObject[] jumpRunes;
    protected GameObject[] speedRunes;

    // Start is called before the first frame update
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        playerTransform = GetComponent<Transform>();
        audioSource = GetComponent<AudioSource>();

        isPlayingWin = false;
        isPlayingJump = false;
        isPlayingSpeed = false;

        currJumpTime = 0f;
        currSpeedTime = 0f;
        effectsString = "";


        //find all jump and speeds runes
        jumpRunes = GameObject.FindGameObjectsWithTag("JumpRune");
        speedRunes = GameObject.FindGameObjectsWithTag("SpeedRune");
    }

    protected override void ComputeVelocity()
    {
        effectsText.text = effectsString;

        if (RectangleFireController.isWin && !isPlayingWin) //The playing of the win sound effect
        {
            audioSource.Play();
            isPlayingWin = true;
            effectsText.text = "";
        }

        foreach (GameObject g in jumpRunes)
        {
            if (g != null)
            {
                RuneController curr = g.GetComponent<RuneController>();
                if (curr.isActive()) //if a jump rune is active, update the jumpTakeOffSpeed and start counting down
                {
                    currJumpTime = curr.getTime(); //The remaining time on this jump rune
                    
                    if (!g.GetComponent<AudioSource>().isPlaying && curr.getTime() == runeTime) //play the jump rune pickup sound effect if this rune is new
                    {
                        g.GetComponent<AudioSource>().Play();
                        isPlayingJump = true;
                    }
                    jumpTakeOffSpeed = 10; //Increase jump speed
                    runeTimer(g, curr, curr.getTime(), g.tag); //Update the rune's timer
                    break;
                }
            }
        }

        foreach (GameObject g in speedRunes)
        {
            if (g != null)
            {
                RuneController curr = g.GetComponent<RuneController>();
                if (curr.isActive()) //if a speed rune is active, update the maxSpeed and start counting down
                {
                    currSpeedTime = curr.getTime();

                    if (!g.GetComponent<AudioSource>().isPlaying && curr.getTime() == runeTime)  //play the speed rune pickup sound effect if this rune is new
                    {
                        g.GetComponent<AudioSource>().Play();
                        isPlayingSpeed = true;
                    }
                    maxSpeed = 10; //increase move speed
                    runeTimer(g, curr, curr.getTime(), g.tag); //Update the rune's timer
                    break;
                }
            }
        }

        Vector2 move = Vector2.zero;
        move.x = Input.GetAxis("Horizontal");

        if(Input.GetButtonDown("Jump") && grounded)
        {
            velocity.y = jumpTakeOffSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if(velocity.y > 0)
            {
                velocity.y = velocity.y * .5f;
            }
        }

        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < 0.01f));
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }

        animator.SetBool("grounded", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;

        generateEffectsString();
    }

    public void generateEffectsString() //Creates the effects string in the upper left of the HUD based on which effects are up
    {
        if(currJumpTime > 0 && currSpeedTime == 0) //Only jump
        {
            effectsString = "+Jump: " + currJumpTime.ToString("0.0");
        } else if(currSpeedTime > 0 && currJumpTime == 0) //Only speed
        {
            effectsString = "+Speed: " + currSpeedTime.ToString("0.0");
        }
        else if(currJumpTime > 0 && currSpeedTime > 0) //Both
        {
            effectsString = "+Speed: " + currSpeedTime.ToString("0.0") + "\n+Jump: " + currJumpTime.ToString("0.0");
        } else //None
        {
            effectsString = "";
        }
    }

    public void runeTimer(GameObject g, RuneController rune, float time, string runeType) //Updates active rune's times and removes their effects
    {
        rune.setTime(rune.getTime() - Time.deltaTime); //update the time of this rune

        if(rune.getTime() <= 0 ) //If a rune's timer is done
        {
            if (runeType == "JumpRune")
            {
                isPlayingJump = false;
                if (jumpRunes.Length > 1) //If g isn't the last jump rune, remove it from the array
                {
                    GameObject[] newJumpRunes = new GameObject[jumpRunes.Length - 1];
                    int curr = 0;
                    for (int i = 0; i < jumpRunes.Length; i++) //copy all runes but g into a temp array
                    {
                        if (!jumpRunes[i].Equals(g)) 
                        {
                            newJumpRunes[curr] = jumpRunes[i];
                            curr++;
                        } 
                    }
                    jumpRunes = newJumpRunes;
                }
                currJumpTime = 0f; 
                jumpTakeOffSpeed = 7; //set jump back to normal
            }
            else if(runeType == "SpeedRune")
            {
                isPlayingSpeed = false;
                if (speedRunes.Length > 1) //If g isn't the last speed rune, remove it from the array
                {
                    GameObject[] newSpeedRunes = new GameObject[speedRunes.Length - 1];
                    int curr = 0;
                    for (int i = 0; i < speedRunes.Length; i++)  //copy all runes but g into a temp array
                    {
                        if (!speedRunes[i].Equals(g))
                        {
                            newSpeedRunes[curr] = speedRunes[i];
                            curr++;
                        }
                    }
                    speedRunes = newSpeedRunes;
                }
                currSpeedTime = 0f;
                maxSpeed = 5; //set speed back to normal
            }            
        }        
    }

}
