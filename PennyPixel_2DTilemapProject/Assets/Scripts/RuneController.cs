﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuneController : MonoBehaviour
{
    protected Transform runeTransform;
    protected float currRuneTime;
    protected float activationDistance; 
    protected bool active = false; //is this rune active?

    protected bool firstRun = true; //is this the first run of the level?
    protected float xInit;
    protected float yInit;
    protected float zInit;

    // Start is called before the first frame update
    void Start()
    { 
        runeTransform = GetComponent<Transform>();
        if(firstRun)
        {
            xInit = runeTransform.localPosition.x;
            yInit = runeTransform.localPosition.y;
            zInit = runeTransform.localPosition.y;
            firstRun = false;
        } else
        {
            runeTransform.localPosition = new Vector3(xInit, yInit, zInit);
        }

        currRuneTime = 5f; //how long the rune effect should stay active for
        activationDistance = 1f; //how far in both x and y the player must be from the rune to activate it
    }

    // Update is called once per frame
    void Update()
    {
        checkRune();
    }

    void checkRune() //Checks if the player is within the activation distance of the rune
    {
        if (Mathf.Abs(PlayerPlatformerController.playerTransform.localPosition.x - runeTransform.position.x) < activationDistance 
            && Mathf.Abs(PlayerPlatformerController.playerTransform.localPosition.y - runeTransform.position.y) < activationDistance 
            && !active && gameObject.activeSelf)
        {
            //Teleport the rune out of view and activate it
            runeTransform.localPosition = new Vector3(100000, 100000, 0); 
            active = true;
        }
    }

    public bool isActive() //returns the activate state of this rune
    {
        return active;
    }

    public void setActive(bool state) //changes the active state of this rune
    {
        gameObject.SetActive(state);
    }

    public float getTime() //gets the remaining time on this rune
    {
        return this.currRuneTime;
    }

    public void setTime(float time) //sets the remaning time on this rune
    {
        this.currRuneTime = time;
    }
}
