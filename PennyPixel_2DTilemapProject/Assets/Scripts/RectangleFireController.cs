﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RectangleFireController : MonoBehaviour
{
    protected float deltaXCurr = 0f; //Stores the fire size over time
    protected float deltaX = 1f; //How fast the fire will grow

    public Text loseText; //"Game Over!"
    public Text restartText; //"Press [SPACE] to restart"
    public Text runText; //"Run!"

    protected float gameOverSlowMo; //The time the player spends in slow-mo after losing
    protected bool isGameOver; //Whether or not the player has lost
    public static bool isWin; //Whether or not the player has won
    protected bool isPlayingGameOver; //Whether or not the game over sound effect is playing
    protected float runCount; //How long the "Run!" text has been on the screen

    protected Transform rectTransform;
    protected AudioSource audioSource;

    public const float rectRadius = 15.5f; //approx. radius of fire rect

    // Start is called before the first frame update
    void Start()
    {
        loseText.text = "";
        restartText.text = "";
        runText.text = "Run!";
        runCount = 0;
        gameOverSlowMo = 2.25f;

        isGameOver = false;
        isPlayingGameOver = false;
        isWin = false;

        Time.timeScale = 1f; //Time scale needs to be reset each restart       

        rectTransform = GetComponent<Transform>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //If more than 3 seconds have passed, remove the runText
        if (runCount < 3) 
        {
            runCount += Time.deltaTime;
        }
        else
        {
            runText.text = "";
        }

        deltaXCurr = Time.deltaTime * deltaX; //How far the fire rect moves forward this frame
        transform.localPosition = new Vector3(transform.localPosition.x + deltaXCurr, transform.localPosition.y, transform.localPosition.z); //Change the position of the fire rect, but only in x  

        float rectEdge = rectTransform.localPosition.x + rectRadius; //The edge of the fire rect

        if (Mathf.Abs(PlayerPlatformerController.playerTransform.localPosition.x - rectEdge) > 7) //If the player is >7 units ahead, speed up the rect to so the game isn't too easy
        {
            deltaX = 3f;
        }
        else //maintain normal speed
        {
            deltaX = 1.7f;
        }

        if (Mathf.Abs(PlayerPlatformerController.playerTransform.localPosition.x - rectEdge) < 0.1 || PlayerPlatformerController.playerTransform.localPosition.y <= -9 && !isWin) //If the player and the edge of the sphere are <0.1 apart, or the player is below y=-9, begin the gameOver process
        {
            
            loseText.text = "Game Over!";
            runText.text = "";
            Time.timeScale = 0.2f;

            isGameOver = true;
        }

        if(Mathf.Abs(PlayerPlatformerController.playerTransform.localPosition.x - 226.5f) < 0.1 && PlayerPlatformerController.playerTransform.localPosition.y > 0) //If the player crosses the finish line, begin the win process
        {
            Time.timeScale = 0.2f;
            loseText.text = "You Win!";

            isWin = true;
        }

        if (isGameOver) //Calculate the remaining slow-mo time
        {
            gameOverSlowMo -= Time.deltaTime * 10;
        }

        if (isWin) //Extend the slow-mo time for a win
        {
            gameOverSlowMo -= Time.deltaTime;
        }

        if(isGameOver && !isPlayingGameOver)
        {
            audioSource.Play();
            isPlayingGameOver = true;
        }

        if (gameOverSlowMo <= 0) //When the slow-mo timer ends, freeze the game and prompt the player to restart
        {
            Time.timeScale = 0f;
            restartText.text = "Press [SPACE] to restart";
            if (Input.GetKeyDown("space")) //restart scene on space press
            {
                SceneManager.LoadScene("Final");
            }
        }
    }
}
