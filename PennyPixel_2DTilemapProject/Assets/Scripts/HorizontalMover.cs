﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalMover : MonoBehaviour
{
    public float transformXRate = .01f;
    public float maxDistance = 5f;
    public string initDirection;

    protected float xInit;
    protected bool direction;

    // Start is called before the first frame update
    void Start()
    {
        xInit = transform.localPosition.x;
        direction = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Move according the initial direction set
        if(initDirection == "R") 
        {
            goesRightFirstTile();
        } 
        else if (initDirection == "L")
        {
            goesLeftFirstTile();
        }
        else
        {
            throw new System.Exception("Invalid initDirection for: " + gameObject.name);
        }
    }

    public void goesRightFirstTile()
    {
        if (direction) //move right
        {
            transform.localPosition = new Vector3(transform.localPosition.x + Time.deltaTime * transformXRate, transform.localPosition.y, transform.localPosition.z);
        }
        else //move left
        {
            transform.localPosition = new Vector3(transform.localPosition.x - Time.deltaTime * transformXRate, transform.localPosition.y, transform.localPosition.z);
        }

        if (transform.localPosition.x > xInit + maxDistance || transform.localPosition.x < xInit) //check if the left or right bounds have been reached
        {
            direction = !direction;
        }
    }

    public void goesLeftFirstTile()
    {
        if (direction) //move left
        {
            transform.localPosition = new Vector3(transform.localPosition.x - Time.deltaTime * transformXRate, transform.localPosition.y, transform.localPosition.z);
        }
        else //move right
        {
            transform.localPosition = new Vector3(transform.localPosition.x + Time.deltaTime * transformXRate, transform.localPosition.y, transform.localPosition.z);
        }

        if (transform.localPosition.x < xInit - maxDistance || transform.localPosition.x > xInit) //check if the left or right bounds have been reached
        {
            direction = !direction; 
        }
    }
}
