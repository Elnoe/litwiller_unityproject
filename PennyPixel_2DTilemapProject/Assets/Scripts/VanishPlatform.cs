﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VanishPlatform : MonoBehaviour
{
    public float toggleTime = 3f; //The interval on which the platform changes state from visible to hidden
    protected float currTime; //How long before the next state change
    protected Vector3 initTransform; //Initial location of the platform
    protected Transform thisTransform; //Transform of this platform
    protected AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        currTime = toggleTime; //Begin the countdown to the next state switch
        thisTransform = GetComponent<Transform>();
        audioSource = GetComponent<AudioSource>();
        initTransform = thisTransform.localPosition; //Save the initial position
    }

    // Update is called once per frame
    void Update()
    {
        if (currTime > 0) //Continue to countdown if there is time remaining
        {
            currTime -= Time.deltaTime;
        }
        else //Switch states
        {
            if (!audioSource.isPlaying && Mathf.Abs(PlayerPlatformerController.playerTransform.localPosition.x - gameObject.transform.localPosition.x) < 20)
            {
                audioSource.Play();
            }
            if (thisTransform.localPosition.z == -1000f) //Hidden --> Visible
            {
                thisTransform.localPosition = new Vector3(initTransform.x, initTransform.y, 0f);
            }
            else //Visible --> Hidden
            {
                thisTransform.localPosition = new Vector3(initTransform.x, initTransform.y, -1000f);
            }
            currTime = toggleTime; //Restart the timer
        }
    }
}
