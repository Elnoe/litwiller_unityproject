﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemRotater : MonoBehaviour
{
    protected Transform thisTransform;
    protected AudioSource audioSource;
    protected bool hasPlayed;

    // Start is called before the first frame update
    void Start()
    {
        thisTransform = GetComponent<Transform>();
        audioSource = GetComponent<AudioSource>();
        hasPlayed = true;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(1, 1, 30) * Time.deltaTime); //Rotate the gem over time

        if(Mathf.Abs(PlayerPlatformerController.playerTransform.localPosition.x - thisTransform.localPosition.x) < 1.75 &&  //pickup gem when player gets close
            Mathf.Abs(PlayerPlatformerController.playerTransform.localPosition.y - thisTransform.localPosition.y) < 1.75)
        {
            thisTransform.localPosition = new Vector3(1000,1000,-1000);
            hasPlayed = false;
        }

        if (!hasPlayed && RectangleFireController.isWin) //play pickup sound
        {
            audioSource.Play();
            hasPlayed = true;
        }
    }
}
